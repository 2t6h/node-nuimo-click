const fs = require("fs");
const SerialPort = require("serialport");

class ClickListener{
	constructor({
		serialPath = "/dev/ttyUSB0",
		logIds = false
	} = {}){
		let port = new SerialPort(serialPath, {
			baudRate: 57600,
			lock: false
		});

		this.byteBuffer = [];
		this.onUpdateStateCbs = [];
		this.logIds = logIds;

		port.on("data", data => {
			for(let i=0; i<data.length; i++){
				this.byteBuffer.push(data[i]);
			}
			this.parseBuffer();
		});
	}

	parseBuffer(){
		while(true){
			let foundMessage = false;
			for(let i=0; i<this.byteBuffer.length; i++){
				let byte = this.byteBuffer[i];
				if(byte == 0x55 && this.byteBuffer.length - i >= 21){
					if(
						this.byteBuffer[i+1] == 0x00 &&
						this.byteBuffer[i+2] == 0x07 &&
						this.byteBuffer[i+3] == 0x07 &&
						this.byteBuffer[i+4] == 0x01 &&
						this.byteBuffer[i+5] == 0x7a
					){
						let foundByte = this.byteBuffer[i+7];
						let idArr = this.byteBuffer.slice(8, 12);
						idArr.reverse();
						let uint8Array = new Uint8Array(idArr);
						let id = new Int32Array(uint8Array.buffer)[0];
						if(this.logIds) console.log("received message from nuimo click with id "+id);
						foundMessage = true;
						this.byteBuffer.splice(0, i+21);
						this.updateState(foundByte, id);
					}
				}
			}
			if(!foundMessage) break;
		}
	}

	updateState(newState, deviceId){
		let nibble1 = newState & 0xF;
		let nibble2 = newState >> 4;
		let pressedButtons = [];
		if(nibble1 != 0) pressedButtons.push(nibble1);
		if(nibble2 != 0) pressedButtons.push(nibble2);
		for(const cb of this.onUpdateStateCbs){
			cb(pressedButtons, deviceId);
		}
	}

	onUpdateState(cb){
		this.onUpdateStateCbs.push(cb);
	}
}

exports.ClickListener = ClickListener;
