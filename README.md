# Usage

```js
const { ClickListener } = require("node-nuimo-click");

let clickListener = new ClickListener({
    serialPath: config.serialPort,
    logIds: config.logIds,
});
clickListener.onUpdateState((pressedButtons, deviceId) => {
    console.log("currently pressed for "+deviceId+":", pressedButtons);
});
```